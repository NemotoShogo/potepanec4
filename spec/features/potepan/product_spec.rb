require 'rails_helper'

RSpec.feature "products", type: :feature do
  let!(:product) { create(:product, taxons: [taxon]) }
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:test_products) { create_list(:product, 4, taxons: [taxon]) }
  let!(:other_product) { create(:product, taxons: [other_taxon]) }
  let(:other_taxon) { create(:taxon, name: "test-bag", taxonomy: taxonomy) }

  before do
    visit potepan_product_path(product.id)
  end

  it "商品名、値段が表示される" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  it "一覧ページに戻るからカテゴリページに戻る" do
    within(".media-body") do
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  it "HOMEボタンからホーム画面に戻る" do
    within(".breadcrumb") do
      click_on "Home"
      expect(current_path).to eq potepan_root_path
    end
  end

  it "関連商品の名前、価格、画像が4つ表示される" do
    expect(page).to have_content test_products.first.name
    expect(page).to have_content test_products.first.display_price
    expect(page).to have_content test_products.first.display_image.attachment_file_name
    expect(page).to have_selector ".productBox", count: 4
  end

  it "関連商品をクリックすると商品の個別ページに飛ぶ" do
    expect(page).to have_link test_products.first.name
    click_link test_products.first.name
    expect(current_path).to eq potepan_product_path(test_products.first.id)
  end

  it "関連しない商品の商品名が表示されない" do
    within(:css, '.productsContent') do
      expect(page).not_to have_content other_product.name
    end
  end
end
