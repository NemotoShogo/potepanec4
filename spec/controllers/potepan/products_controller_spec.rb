require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "product page" do
    let(:product) { create :product }
    let!(:taxon) { create(:taxon) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "productページが表示される " do
      expect(response).to be_successful
    end

    it "renderメソッドが正しく動く" do
      expect(response).to render_template :show
    end

    it "productがbeforeで定義したproductと同じ値である" do
      expect(assigns(:product)).to eq product
    end

    it "related_products(関連商品)が４つ存在する" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
