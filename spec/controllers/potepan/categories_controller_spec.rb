require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "category page" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "renderメソッドが正しく動く" do
      expect(response).to render_template :show
    end

    it "HTTPリクエストが正常に完了する" do
      expect(response).to be_successful
    end
  end
end
